package main

import "fmt"

/*
This defines a struct that can be called as a JSON from inside the JS
*/

type transmitter struct {
}

func (t transmitter) HelloWorld() {
	fmt.Println("Hello World")
}
