UTILITIES = (function() {
  var exports = {};
  exports.updateInterval = 300;

  exports.onResize = function(element, callback) {
    var elementHeight = element.height,
      elementWidth = element.width;
    (function update() {
      if (element.height !== elementHeight || element.width !== elementWidth) {
        elementHeight = element.height;
        elementWidth = element.width;
        callback();
      }
      setTimeout(update, exports.updateInterval);
    })();
  };
  return exports;
})();
