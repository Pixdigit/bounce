//If run without backend set to true
STANDALONE = !("backend" in window);

//Main app source
function app() {
  var exports = {};

  console.log("Startup");
  if (STANDALONE) {
    console.log("Standalone mode");
  } else {
    console.log("App mode");
  }

  drawAxes = (offset, ctx) => {
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(offset.x, 0);
    ctx.lineTo(offset.x, 1);
    ctx.moveTo(0, offset.y);
    ctx.lineTo(1, offset.y);
    ctx.closePath();
    ctx.restore();
    ctx.stroke();
  };

  exports.ctx = $("#preview")
    .find("canvas")[0]
    .getContext("2d");
  exports.ctx.lineWidth = 0.01;
  updateCanvas = function(ctx) {
    ctx.scale(ctx.canvas.width, -ctx.canvas.height);
    ctx.translate(0, -1);
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(0, 0, 1, 1);
    ctx.fillStyle = "#AAAAAA";
    ctx.strokeStyle = "#000000";
    drawAxes({ x: 0.1, y: 0.15 }, ctx);
    console.log("canvas updated");
  };
  updateCanvas(exports.ctx);
  UTILITIES.onResize($(exports.ctx.canvas), () => {
    updateCanvas(exports.ctx);
  });

  if (!STANDALONE) {
    backend.helloWorld();
  }

  return exports;
}

//Run app and make module available to other scripts
var MAIN;
if (!STANDALONE) {
  MAIN = app();
} else {
  window.onload = () => {
    MAIN = app();
  };
}
