package main

import (
	"fmt"
	"io/ioutil"
	"net/url"

	"github.com/zserge/webview"
)

const uiDir = "./ui/"

func loadPage(w webview.WebView) {
	const jqueryUIDir = "jquery-ui-1.12.1.custom/"
	var CSSSourcePaths = [...]string{
		//jqueryUI css
		jqueryUIDir + "jquery-ui.min",
		jqueryUIDir + "jquery-ui.structure.min",
		jqueryUIDir + "jquery-ui.theme.min",
		//custom css
		"interface",
	}
	var JSSourcePaths = [...]string{
		"jquery-3.3.1.min",
		jqueryUIDir + "jquery-ui.min",
		"utilities",
		"app",
	}

	w.Dispatch(func() {
		// Inject CSS
		for _, path := range CSSSourcePaths {
			fullPath := uiDir + path + ".css"
			source, err := ioutil.ReadFile(fullPath)
			if err != nil {
				fmt.Println("Failed to load css: \"" + fullPath + "\"")
				continue
			}
			w.InjectCSS(string(source))
		}
		// Inject JS
		for _, path := range JSSourcePaths {
			fullPath := uiDir + path + ".js"
			source, err := ioutil.ReadFile(fullPath)
			if err != nil {
				fmt.Println("Failed to load js: \"" + fullPath + "\"")
				continue
			}
			w.Eval(string(source))
		}
	})
}

func main() {
	initialPage, _ := ioutil.ReadFile(uiDir + "interface.html")
	w := webview.New(webview.Settings{
		Title:     "Bounce",
		URL:       `data:text/html,` + url.PathEscape(string(initialPage)),
		Debug:     true,
		Resizable: true,
	})
	transmitterStation := &transmitter{}
	w.Bind("backend", transmitterStation)

	loadPage(w)
	w.Run()
}
